
### Transactions Task

## The task

You have been provided with a text file in comma separated format of 991 transactions spread over a month.
The transactions are for multiple accounts and there are multiple types of transaction. The file has the following columns:

+-------------------+----------------------------------------------------------------------------------------------------------------+
| Field             | Description                                                                                                    |
+-------------------+----------------------------------------------------------------------------------------------------------------+
| transactionId     | String representing the id of a transaction                                                                    |
| accountId         | String representing the id of the account which made the transaction                                           |
| transactionDay    | Integer representing the day the transaction was made on (for simplicity we have removed any time information) |
| category          | String representing the type of category of the transaction                                                    |
| transactionAmount | A double representing the value of the transaction                                                             |
+-------------------+----------------------------------------------------------------------------------------------------------------+

Using the data provided, we would like you to answer the following 3 questions which requires calculating some statistics from the data.
The output for each of the questions can either be provided as 3 files or the code can simply println the results to the console.

# Question 1

Calculate the total transaction value for all transactions for each day.

The output should contain one line for each day and each line should include the day and the total value

# Question 2

Calculate the average value of transactions per account for each type of transaction (there are seven in total).
The output should contain one line per account, each line should include the account id and the average value for each
transaction type (ie 7 fields containing the average values).

# Question 3



## Important

The solution must be written Scala, in a functional style. You are able to use any functionality from the standard library in your solution.