package example

import scala.io.Source
import models.Transaction
import scala.collection.immutable.ListMap

object Hello extends App {

  ListMap(Question1.calculateDailyTotal("./data/transactions.csv").toSeq.sortBy(_._1):_*) map (dailyTotal => println(dailyTotal.productIterator.mkString(",")))

  ListMap(Question2.calculateAccountAvgPerType("./data/transactions.csv").toSeq.sortBy(_._1):_*) map
    ((accountWithTransactions) => accountWithTransactions match {
      case (account, typeGroups) => println(account + "," + ListMap(typeGroups.toSeq.sortBy(_._1):_*).values.mkString(","))
    })

  ListMap(Question3.calculateRollingTimeWindow("./src/test/fixtures/scala/example/HelloSpec/sampleTransactionsFor7Days.csv").toSeq.sortBy(_._1):_*) map
    ((accountWithTransactions) => accountWithTransactions match {
      case (accountId, dayGroups) => dayGroups map
        ((dayGroup) => dayGroup match {
          case (day, groupStats) => println(day + "," + accountId + "," + groupStats._2 + "," + groupStats._1 + "," +
            ListMap(groupStats._3.toSeq.sortBy(_._1):_*).values.mkString(","))
        })
    })
}

class Question {

  type Average = Double
  type TotalAmount = Double
  type MaximumValue = Double
  type AccountId = String
  type Category = String
  type Day = Int

  def decodeLine(line: String): Transaction = {
    val values = line.split(",").map(_.trim)
    Transaction(values(0), values(1), values(2).toInt, values(3), values(4).toDouble)
  }

  def readLines(fileName: String): List[String] =
    Source.fromFile(fileName).getLines.toList.drop(1)

  def readCsv(fileName: String): List[Transaction] =
    readLines(fileName) map decodeLine
}

object Question1 extends Question {

  type DailyTotals = Map[Day, TotalAmount]

  def calculateDailyTotal(fileName: String): DailyTotals =
    readCsv(fileName) groupBy (_.transactionDay) mapValues (_.map(_.transactionAmount).sum)
}

object Question2 extends Question {

  type AccountTypeAvg = Map[AccountId, Map[Category, Average]]

  def calculateAccountAvgPerType(fileName: String): AccountTypeAvg =
    readCsv(fileName) groupBy (_.accountId) mapValues (_.groupBy (_.category)) mapValues
      (_.mapValues (ts => ts.map(_.transactionAmount).sum / ts.length))
}

object Question3 extends Question {

  def groupByRollingTime(transactionGroupsByDay: Map[Day, List[Transaction]]): Map[Day, List[Transaction]] = {
    val sortedTransactionGroups = ListMap(transactionGroupsByDay.toSeq.sortWith(_._1 > _._1): _*)

    val lastDay = sortedTransactionGroups.head._1
    val firstDay = sortedTransactionGroups.last._1

    ((firstDay + 5) to (lastDay + 5)).toList.map(day =>
      (day, ((day - 5) to (day - 1)).toList.map(includedDay => transactionGroupsByDay.getOrElse(includedDay, List())).reduceLeft(_ ++ _))
    ).filter(tuple => tuple._2.length > 0).toMap
  }

  def calculateAvg(transactions: List[Transaction]): (Average, List[Transaction]) =
    (
      transactions.map(_.transactionAmount).sum / transactions.length,
      transactions
    )

  def appendMax(currentResult: (Average, List[Transaction])): (Average, MaximumValue, List[Transaction]) =
    (
      currentResult._1,
      currentResult._2.map(_.transactionAmount).max,
      currentResult._2
    )

  def calculateTotalPerType(categories: List[Category], currentResult: (Average, MaximumValue, List[Transaction])): (Average, MaximumValue, Map[Category, TotalAmount]) = {
    val currentCategories = currentResult._3.groupBy(_.category).mapValues(transactions =>
      transactions.map(_.transactionAmount).sum
    )
    (
      currentResult._1,
      currentResult._2,
      categories.map(category => (category, currentCategories.getOrElse(category, 0.0))).toMap
    )
  }

  def extractCategories(transactions: List[Transaction]): List[Category] =
    transactions.groupBy(_.category).keys.toList

  def calculateRollingTimeWindow(fileName: String): Map[AccountId, Map[Day, (Average, MaximumValue, Map[Category, TotalAmount])]] = {
    val transactions = readCsv(fileName)
    val categories = extractCategories(transactions)
    transactions groupBy (_.accountId) mapValues (_.groupBy (_.transactionDay)) mapValues
      groupByRollingTime mapValues (_.mapValues (calculateAvg)) mapValues
      (_.mapValues (appendMax))  mapValues
      (_.mapValues (calculateTotalPerType(categories, _)))
  }
}