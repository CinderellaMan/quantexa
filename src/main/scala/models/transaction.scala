package models

/**
  * Created by emanuel on 28/05/17.
  */
case class Transaction(id: String, accountId: String, transactionDay: Int, category: String, transactionAmount: Double)
