package example

import org.scalatest._

class HelloSpec extends FunSpec {

  describe("Question1") {

    describe("(when empty transactions file passed)") {

      it("should return empty Map") {
        val result = Question1.calculateDailyTotal("./src/test/fixtures/scala/example/HelloSpec/sampleTransactionsEmpty.csv")
        assert(result == Map())
      }
    }

    describe("(when transactions from single day passed)") {

      it("should return Map with single key -> value pair (and right sum of transactions)") {
        val result = Question1.calculateDailyTotal("./src/test/fixtures/scala/example/HelloSpec/sampleTransactionsSingleDay.csv")
        assert(result == Map(1 -> 21.97))
      }
    }

    describe("(when transactions from multiple days passed)") {

      it("should return Map with multiple key -> value pair (and right sum of transactions)") {
        val result = Question1.calculateDailyTotal("./src/test/fixtures/scala/example/HelloSpec/sampleTransactionsMultipleDays.csv")
        assert(result == Map(1 -> 21.97, 2 -> 13.3, 3 -> 0.88))
      }
    }
  }

  describe("Question2") {

    describe("(when empty transactions file passed)") {

      it("should return empty Map") {
        val result = Question2.calculateAccountAvgPerType("./src/test/fixtures/scala/example/HelloSpec/sampleTransactionsEmpty.csv")
        assert(result == Map())
      }
    }

    describe("(when transactions from single account single category passed)") {

      it("should return Map with single key -> value pair where value is another Map of category -> average value") {
        val result = Question2.calculateAccountAvgPerType("./src/test/fixtures/scala/example/HelloSpec/sampleTransactionsSingleAccountAndCategory.csv")
        assert(result == Map("A32" -> Map("DD" -> 1.75)))
      }
    }

    describe("(when transactions from multiple accounts with multiple category passed)") {

      it("should return Map with multiple key(account) -> value pair where value is another Map of category -> average value") {
        val result = Question2.calculateAccountAvgPerType("./src/test/fixtures/scala/example/HelloSpec/sampleTransactionsMultipleAccountAndCategory.csv")
        assert(result == Map("A32" -> Map("AA" -> 7.5, "DD" -> 1.75), "A42" -> Map("EE" -> 5), "A45" -> Map("GG" -> 4)))
      }
    }
  }

  describe("Question3") {

    describe("(when empty transactions file passed)") {

      it("should return empty Map") {
        val result = Question3.calculateRollingTimeWindow("./src/test/fixtures/scala/example/HelloSpec/sampleTransactionsEmpty.csv")
        assert(result == Map())
      }
    }

    describe("(when transactions from multiple accounts with multiple category passed)") {

      it("should return Map with multiple key(account) -> value pair where value is another Map of day -> tuple of results from previous 5 days: average, max and totals per type") {
        val result = Question3.calculateRollingTimeWindow("./src/test/fixtures/scala/example/HelloSpec/sampleTransactionsFor7Days.csv")
        assert(result == Map("A30" ->
                                  Map(10 -> (366.735,619.14,Map(
                                                  "BB" -> 0.0,
                                                  "FF" -> 0.0,
                                                  "GG" -> 0.0,
                                                  "EE" -> 0.0,
                                                  "CC" -> 0.0,
                                                  "DD" -> 733.47)
                                  ), 9 -> (366.735,619.14,Map(
                                                  "BB" -> 0.0,
                                                  "FF" -> 0.0,
                                                  "GG" -> 0.0,
                                                  "EE" -> 0.0, 
                                                  "CC" -> 0.0,
                                                  "DD" -> 733.47)
                                  ), 7 -> (602.1479999999999,958.29,Map(
                                                  "BB" -> 0.0,
                                                  "FF" -> 822.84,
                                                  "GG" -> 1454.4299999999998,
                                                  "EE" -> 0.0,
                                                  "CC" -> 0.0,
                                                  "DD" -> 733.47)
                                  ),11 -> (366.735,619.14,Map(
                                                  "BB" -> 0.0,
                                                  "FF" -> 0.0,
                                                  "GG" -> 0.0,
                                                  "EE" -> 0.0,
                                                  "CC" -> 0.0,
                                                  "DD" -> 733.47)
                                  ), 8 -> (513.1125,822.84,Map(
                                                  "BB" -> 0.0,
                                                  "FF" -> 822.84,
                                                  "GG" -> 496.14,
                                                  "EE" -> 0.0,
                                                  "CC" -> 0.0,
                                                  "DD" -> 733.47)
                                  )
                              ), "A16" -> Map(
                                  10 -> (817.64,873.61,Map(
                                                  "BB" -> 873.61,
                                                  "FF" -> 761.67,
                                                  "GG" -> 0.0,
                                                  "EE" -> 0.0,
                                                  "CC" -> 0.0,
                                                  "DD" -> 0.0)
                                  ), 6 -> (719.3520000000001,874.84,Map(
                                                  "BB" -> 786.14,
                                                  "FF" -> 1636.51,
                                                  "GG" -> 0.0,
                                                  "EE" -> 820.45,
                                                  "CC" -> 353.66,
                                                  "DD" -> 0.0)
                                  ), 9 -> (836.7066666666666,874.84,Map(
                                                  "BB" -> 873.61,
                                                  "FF" -> 1636.51,
                                                  "GG" -> 0.0,
                                                  "EE" -> 0.0,
                                                  "CC" -> 0.0,
                                                  "DD" -> 0.0)
                                  ), 7 -> (836.7066666666666,874.84,Map(
                                                  "BB" -> 873.61,
                                                  "FF" -> 1636.51,
                                                  "GG" -> 0.0,
                                                  "EE" -> 0.0,
                                                  "CC" -> 0.0,
                                                  "DD" -> 0.0)
                                  ), 11 -> (873.61,873.61,Map(
                                                  "BB" -> 873.61,
                                                  "FF" -> 0.0,
                                                  "GG" -> 0.0,
                                                  "EE" -> 0.0,
                                                  "CC" -> 0.0,
                                                  "DD" -> 0.0)
                                  ), 8 -> (836.7066666666666,874.84,Map(
                                                  "BB" -> 873.61,
                                                  "FF" -> 1636.51,
                                                  "GG" -> 0.0,
                                                  "EE" -> 0.0,
                                                  "CC" -> 0.0,
                                                  "DD" -> 0.0)
                                  )
                              ), "A9" -> Map(
                                  7 -> (611.7433333333333,685.45,Map(
                                                  "BB" -> 676.48,
                                                  "FF" -> 0.0,
                                                  "GG" -> 0.0,
                                                  "EE" -> 0.0,
                                                  "CC" -> 0.0,
                                                  "DD" -> 1158.75)
                                  ), 8 -> (685.45,685.45,Map(
                                                  "BB" -> 0.0,
                                                  "FF" -> 0.0,
                                                  "GG" -> 0.0,
                                                  "EE" -> 0.0,
                                                  "CC" -> 0.0,
                                                  "DD" -> 685.45)
                                  ), 9 -> (685.45,685.45,Map(
                                                  "BB" -> 0.0,
                                                  "FF" -> 0.0,
                                                  "GG" -> 0.0,
                                                  "EE" -> 0.0,
                                                  "CC" -> 0.0,
                                                  "DD" -> 685.45)))))
      }
    }
  }
}